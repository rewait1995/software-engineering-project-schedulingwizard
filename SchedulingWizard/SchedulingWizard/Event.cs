﻿////////////////////////////////////////////////
// FileName:          Event.cs
// Namespace:         SchedulingWizard
// Project:           SchedulingWizard
// 
// Created On:        4/22/2017
// Last Modified On:  5/03/2017
////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SchedulingWizard
{
    [DataContract()]
    public class Event
    {
        //member variables
        [DataMember()]
        public string searchString;

        [DataMember()]
        public string short_name;
        [DataMember()]
        public string long_name;
        [DataMember()]
        public string course_code;

        [DataMember()]
        public string room;
        [DataMember()]
        public string building;

        [DataMember()]
        public string StringstartTime;
        [DataMember()]
        public string StringendTime;

        [DataMember()]
        public int IntstartTimeHour;
        [DataMember()]
        public int IntstartTimeMin;

        [DataMember()]
        public int IntendTimeHour;
        [DataMember()]
        public int IntendTimeMin;

        [DataMember()]
        public string day_of_meeting;

        [DataMember()]
        public int? enrollment;
        [DataMember()]
        public int? capacity;

        [DataMember()]
        /*Recitation is an additional class that occurs normally at a different time than the normal 2 or 3 day a week
        course.  The courses that have a recitation need to be treated differently to add and remove both Event objects
        for that course at the same time.  Please see ACCT 202A as an example.  There are two courses listed in the
        DB, one for the normal lecture time on MWF and one for the recitation period on Thursday.*/
        public bool recitation = false;

        [IgnoreDataMember()]
        public ListViewItem item_in_list = null;

        [IgnoreDataMember()]
        public List<Button> buttons;

        [DataMember()]
        public bool onCalendar = false;

        public Event(string shortNm, string longNm, string code, string rm, string blding, string st, string et, string meet, int? enroll, int? cap)
        {
            short_name = shortNm;
            long_name = longNm;
            course_code = code;
            room = rm;
            building = blding;
            StringstartTime = st;
            StringendTime = et;
            day_of_meeting = meet;
            enrollment = enroll;
            capacity = cap;

            // Converts military time to civilian time for the searchString
            if (!short_name.Contains("Custom_Event"))
            {
                time_to_int(st, et);
            }
                

            string civ_time = String.Format("{0:0}:{1:00}:{2:00}", (IntstartTimeHour > 12) ? (IntstartTimeHour % 12) : IntstartTimeHour,
                                                                    IntstartTimeMin,
                                                                    "00");
            string[] searchTerms = new string[6] { "%", short_name, long_name, course_code, civ_time, "%" };
            searchString = String.Join(" ", searchTerms).Replace(" ", "%").ToLower();
        }
        //copy constructor
        public Event(Event cloneEvnt)
        {
            short_name = cloneEvnt.short_name;
            long_name = cloneEvnt.long_name;
            course_code = cloneEvnt.course_code;
            room = cloneEvnt.room;
            building = cloneEvnt.building;

            StringstartTime = cloneEvnt.StringstartTime;
            StringendTime = cloneEvnt.StringendTime;
            IntstartTimeHour = cloneEvnt.IntstartTimeHour;
            IntstartTimeMin = cloneEvnt.IntstartTimeMin;
            IntendTimeHour = cloneEvnt.IntendTimeHour;
            IntendTimeMin = cloneEvnt.IntendTimeMin;

            day_of_meeting = cloneEvnt.day_of_meeting;
            enrollment = cloneEvnt.enrollment;
            capacity = cloneEvnt.capacity;
            searchString = cloneEvnt.searchString;           

            recitation = cloneEvnt.recitation;
            item_in_list = cloneEvnt.item_in_list;
            buttons = cloneEvnt.buttons;
            onCalendar = cloneEvnt.onCalendar;
        }

        public void time_to_int(string st, string et)
        {
            if (st != "NULL" && et != "NULL")
            {
                string[] start = st.Split(':');
                IntstartTimeHour = Convert.ToInt32(start[0]);
                IntstartTimeMin = Convert.ToInt32(start[1]);

                //check that the time is within the correct bounds
                if ((IntstartTimeHour > 24) || (IntstartTimeMin > 59))
                {
                    MessageBox.Show("Cannot be added to the schedule due to a invalid time entry.",
                                "Calendar item cannot be added",
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                }

                string[] end = et.Split(':');
                IntendTimeHour = Convert.ToInt32(end[0]);
                IntendTimeMin = Convert.ToInt32(end[1]);

                //check that the time is within the correct bounds
                if ((IntendTimeHour > 24) || (IntendTimeMin > 59))
                {
                    MessageBox.Show("Cannot be added to the schedule due to a invalid time entry.",
                                "Calendar item cannot be added",
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                }
            }

        }

        //converts from 24 hour clock to AM-PM
        public string military_to_civilian(int time)
        {
            if (time >= 13 || time <= 21)
                return (time % 12).ToString();
            else
                return "";
        }
        //converts from AM-PM to 24 hour clock
        public string civilian_to_military(int time)
        {
            return "";
        }

        public string[] getCivilianTime(string start, string end)
        {
            string civilian_start_time = start;
            string civilian_end_time = end;

            //reformats times on tool tip
            //removes seconds
            //converts to civilian time
            //makes sure time is not null
            if ((start != "NULL") || (end != "NULL"))
            {
                //removes seconds
                string[] remove_start_sec = start.Split(':');
                civilian_start_time = remove_start_sec[0] + ":" + remove_start_sec[1];
                string[] remove_end_sec = end.Split(':');
                civilian_end_time = remove_end_sec[0] + ":" + remove_end_sec[1];

                //converts to civilian time--start time
                if (IntstartTimeHour > 12)
                {
                    civilian_start_time = military_to_civilian(IntstartTimeHour);
                    string[] start_minute = start.Split(':');
                    civilian_start_time += ":" + start_minute[1] + " PM";
                }
                else if (IntstartTimeHour == 12)
                {
                    // string[] start_minute = StringstartTime.Split(':');
                    civilian_start_time += " PM";
                }
                //if not past 12 make it AM
                else { civilian_start_time += " AM"; }


                //converts to civilian time--end time
                if (IntendTimeHour > 12)
                {
                    civilian_end_time = military_to_civilian(IntendTimeHour);
                    string[] end_minute = end.Split(':');
                    civilian_end_time += ":" + end_minute[1] + " PM";
                }
                else if (IntendTimeHour == 12)
                {
                    // string[] end_minute = StringendTime.Split(':');
                    civilian_end_time += " PM";
                }
                //if not past 12 make it AM
                else { civilian_end_time += " AM"; }
            }
            
            //use array of string to return both of the strings
            string[] civilian_time = new string[2];
            civilian_time[0] = civilian_start_time;
            civilian_time[1] = civilian_end_time;

            //returns both strings
            return civilian_time;
        }

        public override bool Equals(Object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            Event otherEvent = (Event)obj;
            bool ret = ((this.course_code == otherEvent.course_code) && (this.day_of_meeting == otherEvent.day_of_meeting));
            return ret;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
