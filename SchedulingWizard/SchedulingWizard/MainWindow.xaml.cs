﻿////////////////////////////////////////////////
// FileName:          MainWindow.xaml.cs
// Namespace:         SchedulingWizard
// Project:           SchedulingWizard
// 
// Created On:        3/19/2017
// Last Modified On:  5/07/2017
////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CourseDBContext;
using System.Runtime.Serialization;
using Microsoft.Win32;
using System.IO;
using System.Xml;
using System.Windows.Markup;
using System.ComponentModel;

namespace SchedulingWizard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Event> DB = new List<Event>();
        Database classes = new Database();
        CourseDBDataContext CourseDB = new CourseDBDataContext();
        CheckBoxControl check_box_control = new CheckBoxControl();

        //list of items in the output list
        public List<Event> inList = new List<Event>();
        List<Event> inCustomList = new List<Event>();

        //list for holding events on the calendar
        List<Event> Events_On_Calendar = new List<Event>();

        // save file dialog for import / export of schedules
        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
        // open file dialog for import / export of schedules
        OpenFileDialog openFileDialog1 = new OpenFileDialog();

        //Calendar Object for interaction with the Calendar class
        Calendar cal = new Calendar();

        bool importing = false;

        public MainWindow()
        {
            if (!ConnectionCheck.CheckForInternetConnection())
            {
                MessageBox.Show("Could not retrieve the courses from the Internet. Please check your connection.", "Connection error",
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                Environment.Exit(0);
            }
            //******************************Initialization*********************************************//
            InitializeComponent();
            check_box_control.Setup();
            DB = classes.create_DB(CourseDB);
            classes.findRecitations(DB);

            for (int i = 0; i < DB.Count; i++)
            {
                Add_Event_to_List_Of_Classes(DB[i]);
            }
            inList = DB.ConvertAll(evnt => new Event(evnt)); //Now recitation only works on the first click

            saveFileDialog1.Filter = "XML Files|*.xml";
            saveFileDialog1.Title = "Save Schedule";
            saveFileDialog1.FileName = "Schedule";

            openFileDialog1.Filter = "XML Files|*.xml";
            openFileDialog1.Title = "Open Schedule";
            openFileDialog1.RestoreDirectory = true;
        }

        private void List_View_Clicked(object sender, MouseButtonEventArgs e)
        {
            Event item = (Event)(sender as ListViewItem).Tag;
            Add_to_Calendar(item, sender);
        }

        //clears normal text as soon as selected
        private void Search_Courses_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Search_Courses.Text == "Search Courses...") // Make sure not to clear contents of search bar
            {
                Search_Courses.Clear();
            }
        }

        //sorts the events in the ListView alphabetically
        public void Sort_List_Of_Classes()
        {
            List_Of_Classes.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("Content", System.ComponentModel.ListSortDirection.Ascending));
            return;
        }

        public void ClearAllCourses()
        {
            List<Event> Events_On_Calendar_Copy = new List<Event>(Events_On_Calendar);
            foreach (Event calendarEvent in Events_On_Calendar_Copy)
            {
                if (calendarEvent.short_name.StartsWith("Custom_Event"))
                {
                    Custom_Event_Removal(calendarEvent);
                }
                else
                {
                    Remove_Event_From_Calendar(calendarEvent);
                }
                
            }
            //Clear all items from ListBox of Custom Events
            List_Of_Custom_Events.Items.Clear();
            inCustomList.Clear();

            this.InvalidateVisual();
        }

        //****************************************Addition Functions*******************************************************//
        public void Add_Event_to_List_Of_Classes(Event classcc)
        {
            classcc.onCalendar = false;
            classcc.time_to_int(classcc.StringstartTime, classcc.StringendTime);
            string[] civilianTime = classcc.getCivilianTime(classcc.StringstartTime, classcc.StringendTime);

            ListViewItem item = new ListViewItem();
            item.Tag = classcc;
            item.Content = System.String.Format("{0} {1}", classcc.course_code, classcc.short_name);
            item.Height = 33;
            item.MouseLeftButtonUp += new MouseButtonEventHandler(List_View_Clicked);
            List_Of_Classes.Items.Add(item);

            //set objects ListViewItem
            classcc.item_in_list = item;


            //formatting the tooltip for special cases in NULL and abnormal entries for building, classroom, and times
            string toolTipContent = classcc.long_name + " \n";
            
            if (classcc.building == "NULL") { toolTipContent += "No Location "; }
            else if (classcc.building == "OFFCP") { toolTipContent += "Off Campus "; }
            else if (classcc.building == "TBA") { toolTipContent += "Location TBA "; }
            else { toolTipContent += classcc.building + " "; }
            
            if (classcc.room == "NULL") { }
            else { toolTipContent += classcc.room; }

            toolTipContent += "    " + classcc.day_of_meeting;

            if (classcc.StringstartTime == "NULL") { toolTipContent += "\nStarts: N/A \nEnds: N/A \n"; }
            else { toolTipContent += "\nStarts: " + civilianTime[0] + "\nEnds: " + civilianTime[1] + "\n"; }

            toolTipContent += classcc.enrollment + " out of " + classcc.capacity + " seats left";


            //set up tooltip for the class-use the string formatted above
            ToolTip tooltip = new ToolTip
            {
                Content = toolTipContent
            };
            item.ToolTip = tooltip;

            return;
        }

        //Adds the events to the calendar passes on args passed in
        Button Put_On_Calendar(Event item, int marginFromTop, int marginFromBottom, int column)
        {
            bool conflict = false;
               
            if (!importing)
            {
                if (cal.conflictCheck(item, Events_On_Calendar)){
                    conflict = true;
                    Event_Tab.IsEnabled = false;
                    Export_Button.IsEnabled = false;
                    Retrieve_Button.IsEnabled = false;
                }
            }
                
            Button newBtn = new Button();
            newBtn.Content = new TextBlock
            {
                Text = item.long_name,
                TextWrapping = TextWrapping.Wrap
            };
            Calendar_Grid.Children.Add(newBtn);


            //only change button color if conflict, default color matches color scheme
            if (!item.short_name.StartsWith("Custom_Event"))
            {
                if (conflict)
                {
                    newBtn.Background = Brushes.LightCoral;
                    newBtn.BorderThickness = new Thickness(5);
                    newBtn.BorderBrush = Brushes.Black;
                }
            }
            //different color for custom events
            else
            {
                if (conflict)
                {
                    newBtn.Background = Brushes.LightCoral;
                    newBtn.BorderThickness = new Thickness(5);
                    newBtn.BorderBrush = Brushes.Black;
                }
                else
                {
                    newBtn.Background = Brushes.Gray;
                    newBtn.BorderBrush = Brushes.Gray;
                }
            }

            for (int k = 0; k < Events_On_Calendar.Count; k++)
            {
                if (item.course_code == Events_On_Calendar[k].course_code)
                {
                    for (int j = 0; j < Events_On_Calendar[k].buttons.Count; j++)
                    {
                        //if somewhere on the calendar another class with the same color has a conflict as well, color it red
                        if (Events_On_Calendar[k].buttons[j].Background == Brushes.LightCoral)
                        {
                            newBtn.Background = Brushes.LightCoral;
                            newBtn.BorderThickness = new Thickness(5);
                            newBtn.BorderBrush = Brushes.Black;
                        }
                    }
                }
            }

            newBtn.FontSize = 12;
            //set text to be white
            newBtn.Foreground = Brushes.White;

            newBtn.Tag = item;
            if (!item.short_name.StartsWith("Custom_Event"))
            {
                //single click to remove
                newBtn.PreviewMouseLeftButtonUp += new MouseButtonEventHandler(Remove_from_Calendar);
            }
            //different remove from calendar function if a custom event
            else
            {
                //single click to remove
                newBtn.PreviewMouseLeftButtonUp += new MouseButtonEventHandler(Remove_Custom_Event_from_Calendar);
            }

            newBtn.Margin = new Thickness(0, marginFromTop, 0, marginFromBottom);

            //sets the height so that the text will show up in the middle of the button
            int height = 0;
            if (marginFromBottom > marginFromTop)
            {
                height = (780 - marginFromBottom) - marginFromTop;

            }
            else
            {
                height = (780 - marginFromTop) - marginFromBottom;
            }

            newBtn.Height = height;

            //this check is only for the bug when the recitaion is not added
            string[] civ_time = { "", "" };
            if ((item.StringstartTime != null) && (item.StringendTime != null))
            {
                civ_time = item.getCivilianTime(item.StringstartTime, item.StringendTime);
            }

            //put tooltips on each of the classes
            if (!item.short_name.StartsWith("Custom_Event"))
            {
                ToolTip tooltip = new ToolTip
                {
                    Content = item.course_code + " \n" + item.building + " " + item.room + "-" + item.day_of_meeting + "\nStarts: " + civ_time[0] + "\nEnds: " + civ_time[1]
                };
                newBtn.ToolTip = tooltip;
            }
            else
            {
                ToolTip tooltip = new ToolTip
                {
                    Content = "Custom Event:" + "\n" + item.day_of_meeting + "\nStarts: " + civ_time[0] + "\nEnds: " + civ_time[1]
                };
                newBtn.ToolTip = tooltip;
            }



            Grid.SetRow(newBtn, 0);
            Grid.SetColumn(newBtn, column);

            return newBtn;
        }

        //revisit
        private void Add_to_Calendar(Event item, object sender, bool init_recitation_param=false)
        {
            Add_Buttons_to_Calendar(item, sender);

            bool init_recitation = (item.recitation || init_recitation_param);

            //accounts for recitation of classes such as meeting MWF but a different time on Tues
            //the recitation flags are used if the same class meets on different days at different times. This will allow both event to be taken from the ListView and then added back to the ListView together
            for (int i = 0; i < inList.Count(); i++)
            {
                if (((item.course_code == inList[i].course_code) && (item.day_of_meeting != inList[i].day_of_meeting) && (item.recitation == true) && (inList[i].recitation == true)))
                {
                    //set recitaion flags
                    item.recitation = false;
                    inList[i].recitation = false;

                    //use the objects ListViewItem as the sender to have it removed from the list
                    Add_to_Calendar(inList[i], inList[i].item_in_list, true);
                }
            }

            //remove from the list holding all Events currently in ListView
            if (item.short_name.StartsWith("Custom_Event"))
            {
                inCustomList.Remove(item);
            }else {
                inList.Remove(item);
                item.recitation = init_recitation;
            }
            item.onCalendar = true;
        }

        void Add_Buttons_to_Calendar(Event item, object sender)
        {
            //for start minutes greater than 0
            int marginFromTop = 0;
            //for end minutes greater than 0;
            int marginFromBottom = 0;

            //sets margins for placing button on calendar based on the start time of the event
            bool validMinute = false;
            bool validDay = false;

            //sets margins for placing button on calendar based on the start time of the event
            marginFromTop = cal.getMarginFromTop(item);
            //sets margins for placing button on calendar based on the end time of the event
            marginFromBottom = cal.getMarginFromBottom(item);

            if ((marginFromTop != 0) || (marginFromBottom != 0))
            {
                validMinute = true;
            }

            if (!validMinute)
            {
                MessageBox.Show(item.long_name + " cannot be added to the schedule due to invalid time entry.",
                                "Calendar item cannot be added",
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                return;
            }

            //get the days placed in the right column(s) (corresponding to the five days of the week)
            List<string> insertedButtons = new List<string>();
            insertedButtons.Clear();
            insertedButtons = cal.getDayButtons(item.day_of_meeting);
            //needs to have currentPosistion start at 1
            int currentPosition = 1;
            List<Button> newList = new List<Button>();

            foreach (string curDay in insertedButtons)
            {
                if (curDay == "day")
                {
                    Button dayButton = Put_On_Calendar(item, marginFromTop, marginFromBottom, currentPosition);
                    newList.Add(dayButton);
                    validDay = true;
                }
                currentPosition++;
            }
            item.buttons = newList;

            bool conflict = false;

            if (!importing)
            {
                if (cal.conflictCheck(item, Events_On_Calendar))
                {
                    conflict = true;
                    Event_Tab.IsEnabled = false;
                    Export_Button.IsEnabled = false;
                    Retrieve_Button.IsEnabled = false;
                }
            }

            //paints the rest of the buttons red
            if ((conflict) && (!importing))
            {
                for (int k = 0; k < Events_On_Calendar.Count; k++)
                {
                    if (item.course_code == Events_On_Calendar[k].course_code)
                    {
                        for (int j = 0; j < Events_On_Calendar[k].buttons.Count; j++)
                        {
                            Events_On_Calendar[k].buttons[j].Background = Brushes.LightCoral;
                            Events_On_Calendar[k].buttons[j].BorderThickness = new Thickness(5);
                            Events_On_Calendar[k].buttons[j].BorderBrush = Brushes.Black;
                        }
                    }
                }
            }

            if (validDay)
            {
                if (!item.short_name.StartsWith("Custom_Event"))
                {
                    //removes the ListViewItem from the List
                    List_Of_Classes.Items.Remove(sender);
                }else
                {
                    List_Of_Custom_Events.Items.Remove(sender);
                }
                 //only add to the calendar if it does not have a null time
                Events_On_Calendar.Add(item);
            }
            //if not valid day of meeting
            else
            {
                MessageBox.Show(item.long_name + " cannot be added to the schedule due to it not having a set time.",
                                "Calendar item cannot be added",
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
            }
        }

        private void Insert_Event_On_List_Of_Classes(Event evt) //rename
        {
            if (Search_Courses.Text == "Search Courses...") // Handles clicking checkbox before typing into search box
            {
                Search_Courses.Clear();
            }

            // matches holds all Events whose searchString contains all of the search terms
            List<Event> matches = new List<Event> { };
            var searchTerms = Search_Courses.Text.ToLower().Split(' ');
            // keeps track of the number of search terms that exist in the searchString
            int hits = 0;
            for (int i = 0; i < searchTerms.Count(); i++)
            {
                if (evt.searchString.Contains(searchTerms[i]))
                {
                    hits++;
                }
            }
            // Adds to matches each Event whose searchString conatins all of the search terms
            if (hits == searchTerms.Count())
            {
                Add_Event_to_List_Of_Classes(evt);
            }

            if ((evt.recitation == true) &&
                (inList.Contains(evt)) &&
                (inList.Count() > List_Of_Classes.Items.Count))
                Add_Event_to_List_Of_Classes(evt);
            else if (evt.recitation == false) {
                check_box_control.Reapply_Checkbox_Filters();
            }
        }


        //****************************************Removal Functions*******************************************************//
        private void Remove_from_Calendar(object sender, MouseButtonEventArgs e)
        {
            Event item = (Event)(sender as Button).Tag;
            //call the remove from Calendar method
            Remove_Event_From_Calendar(item);
        }

        //takes an Event object as an argument
        //goes through the buttons of the Event and removes them from the Calendar
        //recursively removes any classes that meet on different days at different times using the recitation flags
        private void Remove_Event_From_Calendar(Event item)
        {
            item.onCalendar = false;
            //checks if there is a conflict with the button clicked
            if (cal.conflictCheck(item, Events_On_Calendar))
            {
                for (int i = 0; i < Events_On_Calendar.Count; i++)
                {
                    for (int j = 0; j < Events_On_Calendar[i].buttons.Count; j++)
                    {
                        Events_On_Calendar[i].buttons[j].IsEnabled = true;
                    }
                }
            }

            for (int k = 0; k < Events_On_Calendar.Count; k++)
            {
                if (item.course_code == Events_On_Calendar[k].course_code)
                {
                    for (int j = 0; j < Events_On_Calendar[k].buttons.Count; j++)
                    {
                        Calendar_Grid.Children.Remove(Events_On_Calendar[k].buttons[j]);
                    }
                }
            }
            Export_Button.IsEnabled = true;
            Event_Tab.IsEnabled = true;
            Retrieve_Button.IsEnabled = true;

            for (int i = 0; i < item.buttons.Count; i++)
            {
                Calendar_Grid.Children.Remove(item.buttons[i]);
            }
            
            //add to the Events in the list
            inList.Add(item);
            Insert_Event_On_List_Of_Classes(item);

            //remove from items on Calendar
            Events_On_Calendar.Remove(item);


            for (int i = 0; i < Events_On_Calendar.Count(); i++)
            {
                if ((item.recitation==true) &&
                    (Events_On_Calendar[i].recitation==true) &&
                    (item.course_code==Events_On_Calendar[i].course_code))
                {
                    Remove_Event_From_Calendar(Events_On_Calendar[i]);
                }
            }

            if (item.recitation == true)
            {
                    Initiate_Query();
            }

            Sort_List_Of_Classes();

        }


        //****************************************Query and Related Functions*******************************************************//
        private void textBoxTest_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) // If user presses enter while inside the search box
            {
                Initiate_Query("keyDown");
            }
        }

        public void Initiate_Query(string caller="NULL")
        {
            //List_Of_Classes
            if (Search_Courses.Text == "Search Courses...") // Handles clicking checkbox before typing into search box
            {
                Search_Courses.Clear();
            }

            // matches holds all Events whose searchString contains all of the search terms
            List<Event> matches = new List<Event> { };
            var searchTerms = Search_Courses.Text.ToLower().Split(' ');
            foreach (Event evt in DB) {
                // keeps track of the number of search terms that exist in the searchString
                int hits = 0;
                for (int i = 0; i < searchTerms.Count(); i++)
                {
                    if(evt.searchString.Contains("%"+searchTerms[i]))
                    {
                        hits++;
                    }

                }
                // Adds to matches each Event whose searchString conatins all of the search terms
                if (hits == searchTerms.Count())
                {
                    matches.Add(evt);
                }
            }
            //Removes all events from the list
            List_Of_Classes.Items.Clear();

            inList.Clear();
            bool isOnCalendar = false;
            foreach (Event entry in matches)
            {
                //makes sure that an item on the calendar does not get put back in the list
                for (int i = 0; i < Events_On_Calendar.Count(); i++)
                {
                    if (entry.course_code == Events_On_Calendar[i].course_code)
                    {
                        isOnCalendar = true;
                        break;
                    }
                    else
                    {
                        isOnCalendar = false;
                    }
                }
                if (!isOnCalendar)
                {
                    //if not a duplicate, put back into the list
                    Add_Event_to_List_Of_Classes(entry);
                    //puts into list of Events currently in ListView
                    inList.Add(entry);
                }
            }

            // Creates a list to hold events that need to be added to the list of courses (e.g. recitations)

            List<Event> additions = new List<Event>();
            foreach (Event any_evnt in DB)
            {
                bool add_event = false;
                if ((any_evnt.recitation == true) && !(inList.Contains(any_evnt)))
                {
                    foreach (Event evnt_in_list in inList)
                    {
                        if ((any_evnt.course_code==evnt_in_list.course_code) && (evnt_in_list.recitation==true))
                        {
                            additions.Add(any_evnt);
                            add_event = true;
                        }
                    }
                    if (add_event)
                    {
                        Add_Event_to_List_Of_Classes(any_evnt); // recitations are added to the GUI
                        inList.Add(any_evnt); // recitations are added to internal list of classes displayed on the GUI
                    }
                    
                }
            }

            // Resorts the list of classes so they appear in alphabetical order
            Sort_List_Of_Classes();

            check_box_control.Reapply_Checkbox_Filters();

            return;
        }

        // Handles when the user selects the MWF checkbox
        // Removes from the course list all courses that don't occur on either M, W, or F
        public void MWF_Checked(object sender, RoutedEventArgs e)
        {
            check_box_control.Filter_By_MWF();
        }

        // Handles when the user deselects the MWF checkbox
        // Re-searches the database for events that fit the given search criteria
        public void MWF_Unchecked(object sender, RoutedEventArgs e)
        {
            check_box_control.Unfilter_By_MWF();
        }

        // Handles when the user selects the TR checkbox
        // Removes from the course list all courses that don't occur on either Tuesday or Thursday
        public void TTh_Checked(object sender, RoutedEventArgs e)
        {
            check_box_control.Filter_By_TR();
        }

        // Handles when the user deselects the TR checkbox
        // Re-searches the database for events that fit the given search criteria
        public void TTh_Unchecked(object sender, RoutedEventArgs e)
        {
            check_box_control.Unfilter_By_TR();
        }


        //****************************************Import/Export Stuff*******************************************************//
        private void Export_Click(object sender, RoutedEventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == true)
            {
                FileStream fs = (FileStream)saveFileDialog1.OpenFile();
                DataContractSerializer ser = new DataContractSerializer(typeof(SerializableEvents));

                SerializableEvents serEvents = new SerializableEvents(Events_On_Calendar);
                ser.WriteObject(fs, serEvents);
                fs.Close();
            }
        }
        private void Import_Click(object sender, RoutedEventArgs e)
        {
            if (openFileDialog1.ShowDialog() == true)
            {
                if (Events_On_Calendar.Count != 0)
                {
                    MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Import and Clear the Calendar?", "Clear Calendar?", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Information);
                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        ClearAllCourses();
                        this.DoImport();
                    }
                }
                else
                {
                    ClearAllCourses();
                    this.DoImport();
                }
                importing = false;
            }
            for (int i = 0; i < inCustomList.Count; i++)
            {
                this.Add_to_Calendar(inCustomList[i], inCustomList[i].item_in_list);
            }
            
        }

        public void DoImport()
        {
            importing = true;
            //Clear all items from ListBox of Custom Events
            List_Of_Custom_Events.Items.Clear();
            inCustomList.Clear();

            FileStream fs = (FileStream)openFileDialog1.OpenFile();
            DataContractSerializer ser = new DataContractSerializer(typeof(SerializableEvents));

            try
            {
                XmlDictionaryReader xmlReader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                SerializableEvents serEvents = (SerializableEvents)ser.ReadObject(xmlReader, true);
                foreach (Event calendarEvent in serEvents.getEvents())
                {
                    foreach (ListViewItem itemInList in List_Of_Classes.Items)
                    {
                        bool equals = calendarEvent.Equals(itemInList.Tag);
                        if (equals)
                        {
                            calendarEvent.item_in_list = itemInList;
                        }
                    }
                    if (calendarEvent.short_name.StartsWith("Custom_Event"))
                    {
                        inCustomList.Add(calendarEvent);
                    }
                    else
                    {
                        this.Add_to_Calendar(calendarEvent, calendarEvent.item_in_list);
                    }
                    
                }                
            }
            catch(Exception ex)
            {
                MessageBox.Show("Unable to import course data due to invalid XML.",
                                   "Calendar cannot be imported.",
                                   MessageBoxButton.OK,
                                   MessageBoxImage.Information);
            }
            finally
            {
                fs.Close();
                this.InvalidateVisual();
            }            
        }

        //****************************************Custom Event Stuff*******************************************************//
        private void Add_Custom_Event_to_List_Click(object sender, RoutedEventArgs e)
        {
            string days_of_meeting = "";
            if ((bool)Mon_Checked.IsChecked)
            {
                days_of_meeting += "M";
            }
            if ((bool)Tues_Checked.IsChecked)
            {
                days_of_meeting += "T";
            }
            if ((bool)Weds_Checked.IsChecked)
            {
                days_of_meeting += "W";
            }
            if ((bool)Thurs_Checked.IsChecked)
            {
                days_of_meeting += "R";
            }
            if ((bool)Fri_Checked.IsChecked)
            {
                days_of_meeting += "F";
            }
            //used for course_code to set apart from other events
            string newGuid = System.Guid.NewGuid().ToString();

            //short name will be used to identify if it is a Custom Event, with Custom_Event being at the beginning
            //public Event(string shortNm, string longNm, string code, string rm, string blding, string st, string et, string meet, int? enroll, int? cap)
            Event customEvent = new Event("Custom_Event" + "_" + Custom_Event_Name.Text, Custom_Event_Name.Text, newGuid, "", "", Custom_Start_Time_Hour.Text + ":" + Custom_Start_Time_Minutes.Text, Custom_End_Time_Hour.Text + ":" + Custom_End_Time_Minutes.Text, days_of_meeting, 0, 0);
            string but = customEvent.short_name;
            //catches if the input is not integers
            try
            {
                Convert.ToInt32(Custom_Start_Time_Hour.Text);
                Convert.ToInt32(Custom_Start_Time_Minutes.Text);
                Convert.ToInt32(Custom_End_Time_Hour.Text);
                Convert.ToInt32(Custom_End_Time_Minutes.Text);
            }
            catch
            {
                MessageBox.Show("Please enter correctly formatted input.",
                                   "Calendar item cannot be added to list.",
                                   MessageBoxButton.OK,
                                   MessageBoxImage.Information);
                clear_fields();
                return;
            }


            if ((bool)PM_Start.IsChecked)
            {
                int IntstartTimeHour = Convert.ToInt32(Custom_Start_Time_Hour.Text);
                //makes sure 12:00 is not made 24:00
                if (IntstartTimeHour != 12)
                {
                    IntstartTimeHour += 12;
                    string IntstartTimeHour_string = Convert.ToString(IntstartTimeHour);
                    customEvent.StringstartTime = IntstartTimeHour_string + ":" + Custom_Start_Time_Minutes.Text;
                    Custom_Start_Time_Hour.Text = IntstartTimeHour_string;
                }
            }
            if ((bool)PM_End.IsChecked)
            {
                int IntEndTimeHour = Convert.ToInt32(Custom_End_Time_Hour.Text);
                //makes sure 12:00 is not made 24:00
                if (IntEndTimeHour != 12)
                {
                    IntEndTimeHour += 12;
                    string IntendTimeHour_string = Convert.ToString(IntEndTimeHour);
                    customEvent.StringendTime = IntendTimeHour_string + ":" + Custom_End_Time_Minutes.Text;
                    Custom_End_Time_Hour.Text = IntendTimeHour_string;
                }
            }

            //go through error checking, if true continue
            if (!error_checking_for_custom_event(customEvent)) { return; }

            //makes sure that if there is already an item with that name in the list that it does not add a duplicate
            for (int i = 0; i < inCustomList.Count; i++)
            {
                if (inCustomList[i].short_name == customEvent.short_name)
                {
                    MessageBox.Show("Please create a custom event different from a custom event already in the list.",
                                "Calendar item cannot be added to list.",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                    clear_fields();
                    return;
                }
            }
            for (int i = 0; i < Events_On_Calendar.Count; i++)
            {
                if (Events_On_Calendar[i].short_name == customEvent.short_name)
                {
                    MessageBox.Show("Please create a custom event different from a custom event already on the calendar.",
                                "Calendar item cannot be added to list.",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                    clear_fields();
                    return;
                }
            }

            //get time in int after error checking
            customEvent.time_to_int(customEvent.StringstartTime, customEvent.StringendTime);

            //add to list of events in ListBox in Add Custom Event
            inCustomList.Add(customEvent);

            clear_fields();

            ListViewItem Listitem = new ListViewItem();
            Listitem.Tag = customEvent;
            Listitem.Content = System.String.Format(customEvent.long_name);
            Listitem.Height = 45;
            //single click
            Listitem.MouseLeftButtonUp += new MouseButtonEventHandler(List_View_Clicked);
            List_Of_Custom_Events.Items.Add(Listitem);

        }
        private void Remove_Custom_Event_from_Calendar(object sender, MouseButtonEventArgs e)
        {
            Event item = (Event)(sender as Button).Tag;

            Custom_Event_Removal(item);
        }

        private void Custom_Event_Removal(Event item)
        {
            //checks if there is a conflict with the button clicked
            if (cal.conflictCheck(item, Events_On_Calendar))
            {
                for (int i = 0; i < Events_On_Calendar.Count; i++)
                {
                    for (int j = 0; j < Events_On_Calendar[i].buttons.Count; j++)
                    {
                        //reenables all other buttons on calendar
                        Events_On_Calendar[i].buttons[j].IsEnabled = true;
                    }
                }
            }
            //once the conflict has been removed, reenable the ListBox and Export Button
            Export_Button.IsEnabled = true;
            Event_Tab.IsEnabled = true;
            Retrieve_Button.IsEnabled = true;

            for (int i = 0; i < item.buttons.Count; i++)
            {
                Calendar_Grid.Children.Remove(item.buttons[i]);
            }


            //remove from list of Events on Calendar
            Events_On_Calendar.Remove(item);
            //add Event to list of Events in ListBox
            inCustomList.Add(item);

            ListViewItem Listitem = new ListViewItem();
            Listitem.Tag = item;
            Listitem.Content = System.String.Format(item.long_name);
            Listitem.Height = 45;
            Listitem.MouseLeftButtonUp += new MouseButtonEventHandler(List_View_Clicked);
            List_Of_Custom_Events.Items.Add(Listitem);

            //set objects ListViewItem
            item.item_in_list = Listitem;

        }
        public bool error_checking_for_custom_event(Event customEvent)
        {
            //makes sure that there is a name for the event
            if (Custom_Event_Name.Text == "")
            {
                MessageBox.Show("Please enter a name for your custom event.",
                                "Calendar item cannot be added to list.",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                clear_fields();
                return false;
            }

            //make sure that at least one day is checked
            if (!((bool)Mon_Checked.IsChecked) && !((bool)Tues_Checked.IsChecked) && !((bool)Weds_Checked.IsChecked) && !((bool)Thurs_Checked.IsChecked) && !((bool)Fri_Checked.IsChecked))
            {
                MessageBox.Show("Please choose at least one day for your event.",
                                "Calendar item cannot be added to list.",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                clear_fields();
                return false;
            }

            //make sure that the fields for the time have been filled out
            if ((Custom_Start_Time_Hour.Text == "") || (Custom_Start_Time_Minutes.Text == "") || (Custom_End_Time_Hour.Text == "") || (Custom_End_Time_Minutes.Text == ""))
            {
                MessageBox.Show("Please enter a time for your event.",
                                "Calendar item cannot be added to list.",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                clear_fields();
                return false;
            }

            //make sure that the the option is not for PM in the morning with AM in the afternoon
            if (((bool)PM_Start.IsChecked) && ((bool)AM_End.IsChecked))
            {
                MessageBox.Show("Cannot add item with PM time in morning and AM time in afternoon.",
                                "Calendar item cannot be added to list.",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                clear_fields();
                return false;
            }

            if ((Convert.ToInt32(Custom_Start_Time_Hour.Text) < 8) && ((bool)AM_Start.IsChecked))
            {
                MessageBox.Show("Pick a time that is after 8AM.",
                                "Calendar item cannot be added to list.",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                clear_fields();
                return false;
            }
            if (((Convert.ToInt32(Custom_Start_Time_Hour.Text) > 21) && ((bool)PM_Start.IsChecked)) || (((Convert.ToInt32(Custom_End_Time_Hour.Text) > 21) && ((bool)PM_End.IsChecked))))
            {
                MessageBox.Show("Pick a time that is before 9PM.",
                                "Calendar item cannot be added to list.",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                clear_fields();
                return false;
            }
            if (((bool)PM_End.IsChecked) && ((bool)PM_Start.IsChecked))
            {
                if (Convert.ToInt32(Custom_Start_Time_Hour.Text) > (Convert.ToInt32(Custom_End_Time_Hour.Text)))
                {
                    MessageBox.Show("Pick a start time that is before the end time.",
                                    "Calendar item cannot be added to list.",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Information);
                    clear_fields();
                    return false;
                }
            }
            if ((Convert.ToInt32(Custom_Start_Time_Hour.Text) == 12) && ((bool)AM_Start.IsChecked))
            {
                MessageBox.Show("Do not pick a midnight as a time.",
                                "Calendar item cannot be added to list.",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                clear_fields();
                return false;
            }
            if ((Convert.ToInt32(Custom_End_Time_Hour.Text) == 12) && ((bool)AM_End.IsChecked))
            {
                MessageBox.Show("Do not pick a midnight as a time.",
                                "Calendar item cannot be added to list.",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                clear_fields();
                return false;
            }
            if ((Convert.ToInt32(Custom_Start_Time_Hour.Text) == (Convert.ToInt32(Custom_End_Time_Hour.Text))))
            {
                if ((Convert.ToInt32(Custom_Start_Time_Minutes.Text) > (Convert.ToInt32(Custom_End_Time_Minutes.Text))))
                {

                    MessageBox.Show("Pick a end time that is after your start time.",
                                    "Calendar item cannot be added to list.",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Information);
                    clear_fields();
                    return false;
                }
                else if ((Convert.ToInt32(Custom_Start_Time_Minutes.Text) == (Convert.ToInt32(Custom_End_Time_Minutes.Text))))
                {
                    MessageBox.Show("Pick different times for your start and end time.",
                                    "Calendar item cannot be added to list.",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Information);
                    clear_fields();
                    return false;
                }
            }
            if ((Convert.ToInt32(Custom_End_Time_Minutes.Text) > 59) || (Convert.ToInt32(Custom_Start_Time_Minutes.Text) > 59))
            {
                MessageBox.Show("Pick minutes that are less than 60.",
                                "Calendar item cannot be added to list.",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                clear_fields();
                return false;
            }
            if (((bool)AM_Start.IsChecked))
            {
                if ((Convert.ToInt32(Custom_Start_Time_Hour.Text) > 11))
                {
                    MessageBox.Show("Pick a time that is within the bounds of 8AM and 9PM.",
                                    "Calendar item cannot be added to list.",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Information);
                    clear_fields();
                    return false;
                }
            }
            if (((bool)AM_Start.IsChecked) && ((bool)AM_End.IsChecked))
            {
                if ((Convert.ToInt32(Custom_End_Time_Hour.Text) < (Convert.ToInt32(Custom_Start_Time_Hour.Text))))
                {
                    MessageBox.Show("Pick an end time that is after the start time.",
                                    "Calendar item cannot be added to list.",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Information);
                    clear_fields();
                    return false;

                }
            }
            if (((bool)PM_Start.IsChecked) && ((bool)PM_End.IsChecked))
            {
                if ((Convert.ToInt32(Custom_End_Time_Hour.Text) < (Convert.ToInt32(Custom_Start_Time_Hour.Text))))
                {
                    MessageBox.Show("Pick an end time that is after the start time.",
                                    "Calendar item cannot be added to list.",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Information);
                    clear_fields();
                    return false;

                }
            }


            return true;
        }
        public void clear_fields()
        {
            //clears all data fields when new event is added
            Custom_Event_Name.Clear();
            Mon_Checked.IsChecked = false;
            Tues_Checked.IsChecked = false;
            Weds_Checked.IsChecked = false;
            Thurs_Checked.IsChecked = false;
            Fri_Checked.IsChecked = false;
            AM_Start.IsChecked = true;
            AM_End.IsChecked = true;
            Custom_Start_Time_Hour.Clear();
            Custom_Start_Time_Minutes.Clear();
            Custom_End_Time_Hour.Clear();
            Custom_End_Time_Minutes.Clear();
        }

        private void Reset_button_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)MWF_Checkbox.IsChecked)
            {
                MWF_Checkbox.IsChecked = false;
                MWF_Unchecked(sender, e);
            }
            if ((bool)TTh_Checkbox.IsChecked)
            {
                TTh_Checkbox.IsChecked = false;
                TTh_Unchecked(sender, e);
            }

            Search_Courses.Clear();
            Initiate_Query("keydown");
            Search_Courses.Text = "Search Courses...";
        }

        private void Retrieve_Button_Click_1(object sender, RoutedEventArgs e)
        {
            //create instance of retreive class
            Retrieve_CC retreive = new Retrieve_CC();
            //create the window and give parent Window as argument
            retreive.instantiate_window(this);
            //puts the right events in the ListView
            retreive.put_inListView(Events_On_Calendar);
        }
    }

}