﻿////////////////////////////////////////////////
// FileName:          Retrieve_CC.xaml.cs
// Namespace:         SchedulingWizard
// Project:           SchedulingWizard
// 
// Created On:        5/04/2017
// Last Modified On:  5/06/2017
////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


//extra feature:retrieve course code for easy copying
namespace SchedulingWizard
{
    /// <summary>
    /// Interaction logic for Retrive_CC.xaml
    /// </summary>
    public partial class Retrieve_CC : Window
    {
        public Retrieve_CC()
        {
            InitializeComponent();
        }
        public void instantiate_window(MainWindow win)
        {
            //set parent for centering Dialog after being instantiated
            Owner = win;
            Title = "Retrieve Course Codes";
        }
        public void add_ListBox_Item(Event evnt)
        {
            ListBoxItem Listitem = new ListBoxItem();
            Listitem.Tag = evnt;
            Listitem.Height = 45;
            Listitem.FontSize = 14;
            //single click
            Listitem.MouseLeftButtonUp += new MouseButtonEventHandler(copy_CC);
            //if it is not a Custom_Event
            if (!evnt.short_name.StartsWith("Custom_Event"))
            {
                Listitem.Content = System.String.Format(evnt.course_code);
            }
            else
            {
                string short_name = evnt.short_name.Replace("Custom_Event_", "");
                Listitem.Content = System.String.Format(short_name);
            }
            //add to the ListBox
            Course_Code_List.Items.Add(Listitem);

            Course_Code_List.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("Content", System.ComponentModel.ListSortDirection.Ascending));
        }
        private void copy_CC(object sender, RoutedEventArgs e)
        {
            //retrieve ListBoxItem information
            ListBoxItem item = (ListBoxItem)(sender as ListBoxItem);

            //copies contents to string for pasing
            Clipboard.SetText(item.Content.ToString());
        }
        public void put_inListView(List<Event> onCalendar)
        {
            List<Event> inDialog = new List<Event>();
            //goes through each item on the Calendar and adds a ListBoxItem in new window
            bool no_add = false;
            for (int i = 0; i < onCalendar.Count; i++)
            {
                for (int j = 0; j < inDialog.Count; j++)
                {
                    if (inDialog[j].course_code == onCalendar[i].course_code)
                    {
                        //if there is a duplicate, do not re-add it to the ListView
                        no_add = true;
                    }
                }
                //if there was no duplicate, add it to the ListView
                if (!no_add)
                {
                    add_ListBox_Item(onCalendar[i]);
                    inDialog.Add(onCalendar[i]);
                }
                no_add = false;
            }

            ShowDialog();
        }

        private void Close_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
