﻿////////////////////////////////////////////////
// FileName:          Import.cs
// Namespace:         SchedulingWizard
// Project:           SchedulingWizard
// 
// Created On:        4/24/2017
// Last Modified On:  4/25/2017
////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CourseDBContext;
using System.Runtime.Serialization;
using Microsoft.Win32;
using System.IO;
using System.Xml;
using System.Windows.Markup;


//functionality for the import button-serializing the events to be used for importing
namespace SchedulingWizard
{
    public static class Import
    {
        public static SerializableEvents DoImport(FileStream fs, ListBox List_Of_Classes)
        {
            DataContractSerializer ser = new DataContractSerializer(typeof(SerializableEvents));

            XmlDictionaryReader xmlReader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            SerializableEvents serEvents = (SerializableEvents)ser.ReadObject(xmlReader, true);
            foreach (Event calendarEvent in serEvents.getEvents())
            {
                foreach (ListViewItem itemInList in List_Of_Classes.Items)
                {
                    bool equals = calendarEvent.Equals(itemInList.Tag);
                    if (equals)
                    {
                        calendarEvent.item_in_list = itemInList;
                    }
                }
            }
            return serEvents;
        }
    }
}
