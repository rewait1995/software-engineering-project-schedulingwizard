﻿////////////////////////////////////////////////
// FileName:          ConnectionCheck.cs
// Namespace:         SchedulingWizard
// Project:           SchedulingWizard
// 
// Created On:        4/05/2017
// Last Modified On:  4/05/2017
////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

//the user cannot open the app if they do not have internet connection
namespace SchedulingWizard
{
    class ConnectionCheck
    {
        public static bool CheckForInternetConnection()
        {
            // from http://stackoverflow.com/questions/2031824/what-is-the-best-way-to-check-for-internet-connectivity-using-net
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.google.com"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
