﻿////////////////////////////////////////////////
// FileName:          SerializableEvents.cs
// Namespace:         SchedulingWizard
// Project:           SchedulingWizard
// 
// Created On:        4/22/2017
// Last Modified On:  4/22/2017
////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SchedulingWizard
{
    [DataContract]
    public class SerializableEvents
    {
        [DataMember]
        private List<Event> events;

        public SerializableEvents(List<Event> events)
        {
            this.events = events;
        }

        public List<Event> getEvents()
        {
            return events;
        }
    }
}
