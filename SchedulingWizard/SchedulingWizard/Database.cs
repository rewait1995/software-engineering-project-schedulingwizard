﻿////////////////////////////////////////////////
// FileName:          Database.cs
// Namespace:         SchedulingWizard
// Project:           SchedulingWizard
// 
// Created On:        4/24/2017
// Last Modified On:  5/01/2017
////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseDBContext;

namespace SchedulingWizard
{
    public class Database
    {
        public List<Event> create_DB(CourseDBDataContext CourseDB)
        {
            //use a Database context created in the MainWindow file
            List<Event> classes = new List<Event>();

            // Query for companies from Boston
            //grab all courses in the database
            var ccdata = from cc in CourseDB.CourseDBs
                         orderby cc.CourseCode
                         select cc;

            //create an event and add the classes to the list for all courses in the database
            foreach (CourseDB comp in ccdata)
            {
                Event classcc = new Event(comp.ShortTitle, comp.LongTitle, comp.CourseCode, comp.Room, comp.Building, comp.BeginTime, comp.EndTime, comp.Meets, comp.Enrollment, comp.Capacity);
                classes.Add(classcc);
            }
            return classes;
        }

        public void findRecitations(List<Event> DB)
        {
            foreach(Event item in DB)
            {
                for (int i = 0; i < DB.Count(); i++)
                {
                    if (((item.course_code == DB[i].course_code) && (item.day_of_meeting != DB[i].day_of_meeting)))
                    {
                        //make sure the item in the DB is updated
                        DB[i].recitation = true;
                    }
                }
            }
        }
    }
}
