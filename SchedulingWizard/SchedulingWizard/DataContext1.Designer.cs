﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using LinqConnect template.
// Code is generated on: 3/22/2017 4:38:36 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using Devart.Data.Linq;
using Devart.Data.Linq.Mapping;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;

namespace CourseDBContext
{

    [DatabaseAttribute(Name = "coursedb")]
    [ProviderAttribute(typeof(Devart.Data.MySql.Linq.Provider.MySqlDataProvider))]
    public partial class CourseDBDataContext : Devart.Data.Linq.DataContext
    {
        public static CompiledQueryCache compiledQueryCache = CompiledQueryCache.RegisterDataContext(typeof(CourseDBDataContext));
        private static MappingSource mappingSource = new Devart.Data.Linq.Mapping.AttributeMappingSource();

        #region Extensibility Method Definitions
    
        partial void OnCreated();
        partial void OnSubmitError(Devart.Data.Linq.SubmitErrorEventArgs args);

        #endregion

        public CourseDBDataContext() :
        base(GetConnectionString("CourseDBDataContextConnectionString"), mappingSource)
        {
            OnCreated();
        }

        public CourseDBDataContext(MappingSource mappingSource) :
        base(GetConnectionString("CourseDBDataContextConnectionString"), mappingSource)
        {
            OnCreated();
        }

        private static string GetConnectionString(string connectionStringName)
        {
            System.Configuration.ConnectionStringSettings connectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings[connectionStringName];
            if (connectionStringSettings == null)
                throw new InvalidOperationException("Connection string \"" + connectionStringName +"\" could not be found in the configuration file.");
            return connectionStringSettings.ConnectionString;
        }

        public CourseDBDataContext(string connection) :
            base(connection, mappingSource)
        {
          OnCreated();
        }

        public CourseDBDataContext(System.Data.IDbConnection connection) :
            base(connection, mappingSource)
        {
          OnCreated();
        }

        public CourseDBDataContext(string connection, MappingSource mappingSource) :
            base(connection, mappingSource)
        {
          OnCreated();
        }

        public CourseDBDataContext(System.Data.IDbConnection connection, MappingSource mappingSource) :
            base(connection, mappingSource)
        {
          OnCreated();
        }

        public Devart.Data.Linq.Table<CourseDB> CourseDBs
        {
            get
            {
                return this.GetTable<CourseDB>();
            }
        }
    }
}

namespace CourseDBContext
{

    /// <summary>
    /// There are no comments for CourseDBContext.CourseDB in the schema.
    /// </summary>
    [Table(Name = @"CourseDB.CourseDB")]
    public partial class CourseDB
    {
        #pragma warning disable 0649

        private string _CourseCode;

        private string _ShortTitle;

        private string _LongTitle;

        private string _BeginTime;

        private string _EndTime;

        private string _Meets;

        private string _Building;

        private string _Room;

        private System.Nullable<int> _Enrollment;

        private System.Nullable<int> _Capacity;
        #pragma warning restore 0649
    
        #region Extensibility Method Definitions

        partial void OnLoaded();
        partial void OnValidate(ChangeAction action);
        partial void OnCreated();
        partial void OnCourseCodeChanging(string value);
        partial void OnCourseCodeChanged();
        partial void OnShortTitleChanging(string value);
        partial void OnShortTitleChanged();
        partial void OnLongTitleChanging(string value);
        partial void OnLongTitleChanged();
        partial void OnBeginTimeChanging(string value);
        partial void OnBeginTimeChanged();
        partial void OnEndTimeChanging(string value);
        partial void OnEndTimeChanged();
        partial void OnMeetsChanging(string value);
        partial void OnMeetsChanged();
        partial void OnBuildingChanging(string value);
        partial void OnBuildingChanged();
        partial void OnRoomChanging(string value);
        partial void OnRoomChanged();
        partial void OnEnrollmentChanging(System.Nullable<int> value);
        partial void OnEnrollmentChanged();
        partial void OnCapacityChanging(System.Nullable<int> value);
        partial void OnCapacityChanged();
        #endregion

        public CourseDB()
        {
            OnCreated();
        }

    
        /// <summary>
        /// There are no comments for CourseCode in the schema.
        /// </summary>
        [Column(Storage = "_CourseCode", DbType = "VARCHAR(16) NULL", UpdateCheck = UpdateCheck.Never)]
        public string CourseCode
        {
            get
            {
                return this._CourseCode;
            }
            set
            {
                if (this._CourseCode != value)
                {
                    this._CourseCode = value;
                }
            }
        }

    
        /// <summary>
        /// There are no comments for ShortTitle in the schema.
        /// </summary>
        [Column(Storage = "_ShortTitle", DbType = "VARCHAR(15) NULL", UpdateCheck = UpdateCheck.Never)]
        public string ShortTitle
        {
            get
            {
                return this._ShortTitle;
            }
            set
            {
                if (this._ShortTitle != value)
                {
                    this._ShortTitle = value;
                }
            }
        }

    
        /// <summary>
        /// There are no comments for LongTitle in the schema.
        /// </summary>
        [Column(Storage = "_LongTitle", DbType = "VARCHAR(35) NULL", UpdateCheck = UpdateCheck.Never)]
        public string LongTitle
        {
            get
            {
                return this._LongTitle;
            }
            set
            {
                if (this._LongTitle != value)
                {
                    this._LongTitle = value;
                }
            }
        }

    
        /// <summary>
        /// There are no comments for BeginTime in the schema.
        /// </summary>
        [Column(Storage = "_BeginTime", DbType = "VARCHAR(8) NULL", UpdateCheck = UpdateCheck.Never)]
        public string BeginTime
        {
            get
            {
                return this._BeginTime;
            }
            set
            {
                if (this._BeginTime != value)
                {
                    this._BeginTime = value;
                }
            }
        }

    
        /// <summary>
        /// There are no comments for EndTime in the schema.
        /// </summary>
        [Column(Storage = "_EndTime", DbType = "VARCHAR(8) NULL", UpdateCheck = UpdateCheck.Never)]
        public string EndTime
        {
            get
            {
                return this._EndTime;
            }
            set
            {
                if (this._EndTime != value)
                {
                    this._EndTime = value;
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Meets in the schema.
        /// </summary>
        [Column(Storage = "_Meets", DbType = "VARCHAR(4) NULL", UpdateCheck = UpdateCheck.Never)]
        public string Meets
        {
            get
            {
                return this._Meets;
            }
            set
            {
                if (this._Meets != value)
                {
                    this._Meets = value;
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Building in the schema.
        /// </summary>
        [Column(Storage = "_Building", DbType = "VARCHAR(5) NULL", UpdateCheck = UpdateCheck.Never)]
        public string Building
        {
            get
            {
                return this._Building;
            }
            set
            {
                if (this._Building != value)
                {
                    this._Building = value;
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Room in the schema.
        /// </summary>
        [Column(Storage = "_Room", DbType = "VARCHAR(4) NULL", UpdateCheck = UpdateCheck.Never)]
        public string Room
        {
            get
            {
                return this._Room;
            }
            set
            {
                if (this._Room != value)
                {
                    this._Room = value;
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Enrollment in the schema.
        /// </summary>
        [Column(Storage = "_Enrollment", DbType = "INT(11) NULL", UpdateCheck = UpdateCheck.Never)]
        public System.Nullable<int> Enrollment
        {
            get
            {
                return this._Enrollment;
            }
            set
            {
                if (this._Enrollment != value)
                {
                    this._Enrollment = value;
                }
            }
        }

    
        /// <summary>
        /// There are no comments for Capacity in the schema.
        /// </summary>
        [Column(Storage = "_Capacity", DbType = "INT(11) NULL", UpdateCheck = UpdateCheck.Never)]
        public System.Nullable<int> Capacity
        {
            get
            {
                return this._Capacity;
            }
            set
            {
                if (this._Capacity != value)
                {
                    this._Capacity = value;
                }
            }
        }
    }

}
