﻿////////////////////////////////////////////////
// FileName:          EventListUnitTests.cs
// Namespace:         SchedulingWizard
// Project:           SchedulingWizardUnitTests
// 
// Created On:        4/24/2017
// Last Modified On:  4/26/2017
////////////////////////////////////////////////


using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Runtime.Serialization;
using Microsoft.Win32;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using CourseDBContext;
using System.Linq;
using SchedulingWizard;


//test that none of the course DB entries are NULL
namespace SchedulingWizardUnitTests
{
    [TestClass]
    public class EventListUnitTests
    {
        public List<Event> DB = new List<Event>();
        Database classes = new Database();
        CourseDBDataContext CourseDB = new CourseDBDataContext();
        [TestMethod]
        public void CreateDBTestMethod()
        {
            DB = classes.create_DB(CourseDB);
            for (int i = 0; i < DB.Count; i++)
            {
                Assert.IsNotNull(DB[i].course_code);
            }
        }
    }
}
